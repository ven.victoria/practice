﻿using Instantly.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.ViewModels
{
    public class OrdersListModel
    {
        public IEnumerable<Order> Orders { get; set; }
        public string Title { get; set; }
        public double? PriceFrom { get; set; }
        public double? PriceTo { get; set; }
        public SelectList Tags { get; set; }
        public string City { get; set; }
    }
}
