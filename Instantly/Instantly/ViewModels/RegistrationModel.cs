﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.ViewModels
{
    public class RegistrationModel
    {
        [Required(ErrorMessage = "Не вказаний Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не вказаний пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Паролі відрізняються")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Не вказане ім'я")]
        public string FIO { get; set; }
        [Required(ErrorMessage = "Не вказана дата народження")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Birthdate { get; set; }
        public string Photo { get; set; }
        public string City { get; set; }
    }
}
