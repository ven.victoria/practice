﻿using Instantly.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.Data
{
    public static class DbInitializer
    {
        public static void Initialize(InstantlyContext context)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any())
                return;

            var users = new User[]
            {
                new User { Email = "admin", Password = "admin", FIO = "admin", Birthdate = DateTime.Now.Date, Photo = "/Files/acton1_0.jpg", City = "San Francisco", Role = "admin" },
                new User { Email = "user1", Password = "user1", FIO = "Kravchenko O.A.", Birthdate = DateTime.Now.Date, City = "Berdychiv" }
            };
            foreach (User u in users)
                context.Users.Add(u);
            context.SaveChanges();

            var tags = new Tag[]
            {
                new Tag { Name = "IT" },
                new Tag { Name = "Електроніка" },
                new Tag { Name = "Кур\'єрскі послуги" },
                new Tag { Name = "Будівництво" },
                new Tag { Name = "Ремонт" },
                new Tag { Name = "Побутові послуги" },
                new Tag { Name = "Логістика" },
                new Tag { Name = "Меблі" },
                new Tag { Name = "Реклама" },
                new Tag { Name = "Переклад" },
                new Tag { Name = "Репетиторство" },
                new Tag { Name = "Краса та здоров\'я" },
                new Tag { Name = "Інше" }
            };
            foreach (Tag t in tags)
                context.Tags.Add(t);
            context.SaveChanges();

            context.OrderStatuses.Add(new OrderStatus { Name = "Нове" });
            context.SaveChanges();
            var orderstatuses = new OrderStatus[]
            {
                new OrderStatus { Name = "Розміщене" },
                new OrderStatus { Name = "На виконанні" },
                new OrderStatus { Name = "Виконане" },
                new OrderStatus { Name = "Закрите" },
                new OrderStatus { Name = "В архіві" }
            };
            foreach (OrderStatus os in orderstatuses)
                context.OrderStatuses.Add(os);
            context.SaveChanges();

            var orders = new Order[]
            {
                new Order { ClientUserId = 2, Title = "Створити сайт", About = "тестове замовлення на створення сайту моментальних завдань", Price = 1480.0, TagId = 1, OrderStatusId = 1 }
            };
            foreach (Order o in orders)
                context.Orders.Add(o);
            context.SaveChanges();

            var orderservices = new OrderService[]
            {
                new OrderService { ExecutionStart = DateTime.Now.Date.AddDays(2), OrderId = 1, ExecutorUserId = 1 }
            };
            foreach (OrderService os in orderservices)
                context.OrderServices.Add(os);
            context.SaveChanges();

            var userreviews = new UserReview[]
            {
                //new UserReview { Rating = Rating.C, Text = "loshara", TargetUserId = 2, AuthorUserId = 1 }
            };
            foreach (UserReview us in userreviews)
                context.UserReviews.Add(us);
            context.SaveChanges();
        }
    }
}
