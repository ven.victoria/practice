﻿using Microsoft.EntityFrameworkCore;
using Instantly.Models;
using System.Linq;

namespace Instantly.Data
{
    public class InstantlyContext : DbContext
    {
        public InstantlyContext(DbContextOptions<InstantlyContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<UserTag> UserTags { get; set; }
        public DbSet<UserReview> UserReviews { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<OrderService> OrderServices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
            modelBuilder.Entity<User>()
                .HasMany(e => e.UserReviewsAsTarget)
                .WithOne();
            modelBuilder.Entity<User>()
                .HasMany(e => e.UserReviewsAsAuthor)
                .WithOne();
            modelBuilder.Entity<UserReview>()
                .HasOne(e => e.AuthorUser)
                .WithMany();
            modelBuilder.Entity<UserReview>()
                .HasOne(e => e.TargetUser)
                .WithMany();
            */
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<UserTag>().ToTable("UserTag");
            modelBuilder.Entity<UserReview>().ToTable("UserReview");
            modelBuilder.Entity<Tag>().ToTable("Tag");
            modelBuilder.Entity<OrderStatus>().ToTable("OrderStatus");
            modelBuilder.Entity<Attachment>().ToTable("Attachment");
            modelBuilder.Entity<OrderService>().ToTable("OrderService");
            
            /*
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            */
        }
    }
}
