﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.Models
{
    public class OrderService
    {
        public int OrderServiceId { get; set; }
        public DateTime ExecutionStart { get; set; }
        public DateTime? ExecutionEnd { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int? ExecutorUserId { get; set; }
        public User ExecutorUser { get; set; }
    }
}
