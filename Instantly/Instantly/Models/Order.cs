﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Instantly.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required(ErrorMessage = "Не вказаний заголовок")]
        public string Title { get; set; }
        public string About { get; set; }
        [Required(ErrorMessage = "Не вказана ціна")]
        public double Price { get; set; } = 0.0;
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public DateTime CreationDateTime { get; set; } = DateTime.Now;
        public int? AttachmentId { get; set; }
        public Attachment Attachment { get; set; }
        public int OrderStatusId { get; set; } = 1;
        public OrderStatus OrderStatus { get; set; }
        [Required(ErrorMessage = "Не вказана категорія")]
        public int TagId { get; set; }
        public Tag Tag { get; set; }
        public int ClientUserId { get; set; }
        public User ClientUser { get; set; }
        public OrderService OrderService { get; set; }
    }
}
