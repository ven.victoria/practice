﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FIO { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Birthdate { get; set; }
        public string Photo { get; set; }
        public string City { get; set; }
        public string Role { get; set; } = "user"; //user OR admin
        public ICollection<UserTag> UserTags { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<OrderService> OrderServices { get; set; }
        //[ForeignKey("TargetUserId")]
        public virtual ICollection<UserReview> UserReviewsAsTarget { get; set; }
        //[ForeignKey("AuthorUserId")]
        public virtual ICollection<UserReview> UserReviewsAsAuthor { get; set; }
    }
}
