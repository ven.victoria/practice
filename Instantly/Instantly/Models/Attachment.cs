﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.Models
{
    public class Attachment
    {
        public int AttachmentId { get; set; }
        public string Caption { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
