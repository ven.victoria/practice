﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Instantly.Models
{
    public enum Rating { A = 5, B = 4, C = 3, D = 2, E = 1}
    public class UserReview
    {
        public int UserReviewId { get; set; }
        public string Text { get; set; }
        public Rating? Rating { get; set; }
        public int TargetUserId { get; set; }
        //[ForeignKey("TargetUserId")]
        [InverseProperty("UserReviewsAsTarget")]
        public virtual User TargetUser { get; set; }
        public int? AuthorUserId { get; set; }
        //[ForeignKey("AuthorUserId")]
        [InverseProperty("UserReviewsAsAuthor")]
        public virtual User AuthorUser { get; set; }
    }
}
