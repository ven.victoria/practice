﻿using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Instantly.Data;
using Instantly.Models;
using Instantly.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Instantly.Controllers
{
    public class AccountController : Controller
    {
        private readonly InstantlyContext _context;
        private readonly IWebHostEnvironment _appEnvironment;

        public AccountController(InstantlyContext context, IWebHostEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel loginUser)
        {
            if (ModelState.IsValid)
            {
                User user = await _context.Users.FirstOrDefaultAsync(u => u.Email == loginUser.Email && u.Password == loginUser.Password);
                if (user != null)
                {
                    await Authenticate(user);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некоректний email або пароль");
            }
            return View(loginUser);
        }

        [HttpGet]
        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration(RegistrationModel registerUser, IFormFile uploadedPhoto)
        {
            if (ModelState.IsValid)
            {
                User user = await _context.Users.FirstOrDefaultAsync(u => u.Email == registerUser.Email);
                string photoPath = null;
                if (user == null)
                {
                    if (uploadedPhoto != null)
                    {
                        photoPath = "/Files/" + uploadedPhoto.FileName;
                        using (var fileStream = new FileStream(_appEnvironment.WebRootPath + photoPath, FileMode.Create))
                        {
                            await uploadedPhoto.CopyToAsync(fileStream);
                        }
                    }
                    user = new User
                    {
                        Email = registerUser.Email,
                        Password = registerUser.Password,
                        FIO = registerUser.FIO,
                        Birthdate = registerUser.Birthdate,
                        Photo = photoPath,
                        City = registerUser.City
                    };
                    _context.Users.Add(user);
                    await _context.SaveChangesAsync();
                    await Authenticate(user);
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Такий користувач вже існує");
            }
            return View(registerUser);
        }

        [Authorize]
        //[Route("[controller]/[action]")]
        //[Route("[controller]/[action]/{page}")]
        [Route("[controller]/[action]/{useremail}")]
        [Route("[controller]/[action]/{useremail}/{page}")]
        public async Task<IActionResult> Profile(string useremail, string page)
        {
            string userEmail = useremail ?? User.Identity.Name;

            User user = await _context.Users
                .Include(u => u.UserTags)
                .Include(u => u.Orders).ThenInclude(o => o.OrderStatus)
                .Include(u => u.UserReviewsAsTarget).ThenInclude(r => r.AuthorUser)
                .FirstOrDefaultAsync(u => u.Email == userEmail);

            ViewBag.RenderPage = page;
            return View(user);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("[controller]/[action]/{useremail}")]
        [Route("[controller]/[action]/{useremail}/{page}")]
        public async Task<IActionResult> Profile([Bind("UserId,Email,Password,FIO,Birthdate,City")] User user)
        {
            /*
            string userEmail = User.Identity.Name;

            User user = await _context.Users
                .Include(u => u.UserTags)
                .Include(u => u.Orders).ThenInclude(o => o.OrderStatus)
                .Include(u => u.UserReviewsAsTarget).ThenInclude(r => r.AuthorUser)
                .FirstOrDefaultAsync(u => u.Email == userEmail);

            ViewBag.RenderPage = page;
            return View(user);
            */
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Profile", new { page = "Orders"});
            }
            return View(user);
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}