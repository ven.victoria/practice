﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Instantly.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Security.Claims;
using Instantly.ViewModels;

namespace Instantly.Data
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly InstantlyContext _context;
        private readonly IWebHostEnvironment _appEnvironment;

        public OrdersController(InstantlyContext context, IWebHostEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        // GET: Orders
        /*
        public async Task<IActionResult> Index()
        {
            var instantlyContext = _context.Orders.Include(o => o.Attachment).Include(o => o.ClientUser).Include(o => o.OrderStatus).Include(o => o.Tag);
            return View(await instantlyContext.ToListAsync());
        }
        */
        //public IActionResult Index([Bind("Title","PriceFrom","PriceTo","Tags","City")] OrdersListModel olm)
        public IActionResult Index(string title, double? priceFrom, double? priceTo, int? tag, string city, string action)
        {
            if (action == "Очистити")
            {
                ViewBag.OrderTitle = null;
                ViewBag.PriceFrom = null;
                ViewBag.PriceTo = null;
                ViewBag.Tag = null;
                ViewBag.City = null;
                IQueryable<Order> orders_ = _context.Orders.Include(o => o.Tag).Include(o => o.ClientUser);
                List<Tag> tags_ = _context.Tags.ToList();
                tags_.Insert(0, new Tag { TagId = 0, Name = "Всі" });
                OrdersListModel viewModel_ = new OrdersListModel
                {
                    Orders = orders_.ToList(),
                    Title = title,
                    PriceFrom = priceFrom,
                    PriceTo = priceTo,
                    Tags = new SelectList(tags_, "TagId", "Name"),
                    City = city
                };
                return View(viewModel_);
            }


            ViewBag.OrderTitle = title;
            ViewBag.PriceFrom = priceFrom;
            ViewBag.PriceTo = priceTo;
            ViewBag.Tag = tag;
            ViewBag.City = city;

            IQueryable<Order> orders = _context.Orders.Include(o => o.Tag).Include(o => o.ClientUser);

            if (!String.IsNullOrEmpty(title))
                orders = orders.Where(o => o.Title.Contains(title));
            if (tag != null && tag != 0)
                orders = orders.Where(o => o.TagId == tag);
            if (priceFrom != null)
                orders = orders.Where(o => o.Price >= priceFrom);
            if (priceTo != null)
                orders = orders.Where(o => o.Price <= priceTo);
            if (!String.IsNullOrEmpty(city))
                orders = orders.Where(o => o.ClientUser.City.Contains(city));

            List<Tag> tags = _context.Tags.ToList();
            tags.Insert(0, new Tag { TagId = 0, Name = "Всі" });
            OrdersListModel viewModel = new OrdersListModel
            {
                Orders = orders.ToList(),
                Title = title,
                PriceFrom = priceFrom,
                PriceTo = priceTo,
                Tags = new SelectList(tags, "TagId", "Name"),
                City = city
            };
            return View(viewModel);
        }

        public async Task<IActionResult> UserOrders()
        {
            var instantlyContext = _context.Orders
                .Include(o => o.Attachment)
                .Include(o => o.ClientUser)
                .Include(o => o.OrderStatus)
                .Include(o => o.Tag)
                .Where(o => o.ClientUser.Email == User.Identity.Name);
            return View(await instantlyContext.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Attachment)
                .Include(o => o.ClientUser)
                .Include(o => o.OrderStatus)
                .Include(o => o.Tag)
                .Include(o => o.ClientUser.UserReviewsAsTarget)
                .FirstOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }
            int sumOfRevies = 0;
            foreach (UserReview us in order.ClientUser.UserReviewsAsTarget)
                sumOfRevies += (int)us.Rating;
            
            ViewBag.GeneralUserRating = String.Format("{0:0.00}", (double)sumOfRevies / order.ClientUser.UserReviewsAsTarget.Count);
            Random rand = new Random();
            ViewBag.ViewsCount = rand.Next(0, 200);

            return View(order);
        }

        // GET: Orders/Create
        public async Task<IActionResult> Create()
        {
            //ViewData["AttachmentId"] = new SelectList(_context.Attachments, "AttachmentId", "AttachmentId");
            //ViewData["ClientUserId"] = new SelectList(_context.Users, "UserId", "Email");
            ViewBag.ClientUserId = (await _context.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name)).UserId;
            //ViewData["OrderStatusId"] = new SelectList(_context.OrderStatuses, "OrderStatusId", "Name");
            ViewData["TagId"] = new SelectList(_context.Tags, "TagId", "Name");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderId,Title,About,Price,TagId,ClientUserId")] Order order, IFormFile uploadedFile)
        {
            if (ModelState.IsValid)
            {
                if(uploadedFile != null)
                {
                    string filePath = "/Files/" + uploadedFile.FileName;
                    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + filePath, FileMode.Create))
                    {
                        await uploadedFile.CopyToAsync(fileStream);
                    }
                    Attachment attachment = new Attachment { Caption = uploadedFile.FileName, Path = filePath, Type = uploadedFile.FileName.Substring(uploadedFile.FileName.LastIndexOf('.') + 1) };
                    _context.Add(attachment);
                    _context.SaveChanges();
                    order.AttachmentId = attachment.AttachmentId;
                }
                _context.Add(order);
                await _context.SaveChangesAsync();
                //return RedirectToAction(nameof(Index));
                return RedirectToAction("UserOrders", "Orders");
            }
            ViewBag.ClientUserId = (await _context.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name)).UserId;
            ViewData["TagId"] = new SelectList(_context.Tags, "TagId", "Name", order.TagId);
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            int clientId = (await _context.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name)).UserId;
            //&& !(User.Identity as ClaimsIdentity).FindFirst("Role").ToString().Equals("admin")
            
            if (order.ClientUserId != clientId && !User.IsInRole("admin"))
            {
                return StatusCode(403);
            }
            
            /*
            ViewData["AttachmentId"] = new SelectList(_context.Attachments, "AttachmentId", "AttachmentId", order.AttachmentId);
            ViewData["ClientUserId"] = new SelectList(_context.Users, "UserId", "UserId", order.ClientUserId);
            ViewData["OrderStatusId"] = new SelectList(_context.OrderStatuses, "OrderStatusId", "OrderStatusId", order.OrderStatusId);
            ViewData["TagId"] = new SelectList(_context.Tags, "TagId", "TagId", order.TagId);
            */
            ViewBag.ClientUserId = clientId;
            ViewData["TagId"] = new SelectList(_context.Tags, "TagId", "Name", order.TagId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("OrderId,Title,About,AttachmentId,OrderStatusId,TagId,ClientUserId")] Order order)
        public async Task<IActionResult> Edit(int id, [Bind("OrderId,Title,About,Price,TagId,ClientUserId")] Order order, IFormFile uploadedFile)
        {
            if (id != order.OrderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (uploadedFile != null)
                    {
                        string filePath = "/Files/" + uploadedFile.FileName;
                        Attachment attachment;
                        if (!AttachmentExists(filePath))
                        {
                            using (var fileStream = new FileStream(_appEnvironment.WebRootPath + filePath, FileMode.Create))
                            {
                                await uploadedFile.CopyToAsync(fileStream);
                            }
                            attachment = new Attachment { Caption = uploadedFile.FileName, Path = filePath, Type = uploadedFile.FileName.Substring(uploadedFile.FileName.LastIndexOf('.') + 1) };
                            _context.Add(attachment);
                            _context.SaveChanges();
                        }
                        else
                        {
                            attachment = _context.Attachments.FirstOrDefault(a => a.Path.Equals(filePath));
                        }
                        order.AttachmentId = attachment.AttachmentId;
                    }
                    else
                    {
                        Order beforeUpdate = _context.Orders
                            .AsNoTracking()
                            .FirstOrDefault(o => o.OrderId == id);
                        order.AttachmentId = beforeUpdate.AttachmentId;
                    }
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.OrderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(UserOrders));
            }
            /*
            ViewData["AttachmentId"] = new SelectList(_context.Attachments, "AttachmentId", "AttachmentId", order.AttachmentId);
            ViewData["ClientUserId"] = new SelectList(_context.Users, "UserId", "UserId", order.ClientUserId);
            ViewData["OrderStatusId"] = new SelectList(_context.OrderStatuses, "OrderStatusId", "OrderStatusId", order.OrderStatusId);
            ViewData["TagId"] = new SelectList(_context.Tags, "TagId", "TagId", order.TagId);
            */
            ViewBag.ClientUserId = (await _context.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name)).UserId;
            ViewData["TagId"] = new SelectList(_context.Tags, "TagId", "Name", order.TagId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Attachment)
                .Include(o => o.ClientUser)
                .Include(o => o.OrderStatus)
                .Include(o => o.Tag)
                .FirstOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.FindAsync(id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.OrderId == id);
        }

        private bool AttachmentExists(string path)
        {
            return _context.Attachments.Any(e => e.Path.Equals(path));
        }
    }
}
